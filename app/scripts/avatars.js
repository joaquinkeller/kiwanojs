'use strict';
var  Avatar;
var map, google;
var AvatarSet={
    set:{},
    nbNbors:function() {
        var nb = 0, uid;
        for (uid in AvatarSet.set) {
            if (AvatarSet.set.hasOwnProperty(uid)) {nb++;}
        }
        return nb-1;
    },
    redraw:function(){
        for(var uid in AvatarSet.set){if (AvatarSet.set.hasOwnProperty(uid)){
                var avt = AvatarSet.set[uid];
                avt.redraw();
            }
        }
    },
    removeAvt:function(uid){
        var avt;
        if(AvatarSet.set.hasOwnProperty(uid)){
            avt= AvatarSet.set[uid];
        } else {return;}
        avt.remove();
        delete AvatarSet.set[uid];
        $('#NbNbors')
            .html(''+
                AvatarSet.nbNbors());

    },
    removeAllBut:function(keepList){
        for(var uid in AvatarSet.set){if (AvatarSet.set.hasOwnProperty(uid)){
                AvatarSet.set[uid].keepIt=false;
            }
        }
        for(var i, len=keepList.length;i<len;i++){
            uid=keepList[i];
            if (AvatarSet.set.hasOwnProperty(uid)){
                AvatarSet.set[uid].keepIt=true;
            }
        }
        var toBeRemoved=[];
        for(uid in AvatarSet.set){if (AvatarSet.set.hasOwnProperty(uid)){
                if(!AvatarSet.set[uid].keepIt){toBeRemoved.push(uid);}
            }
        }
        for(i, len=toBeRemoved.length;i<len;i++){
            uid=toBeRemoved[i];
            AvatarSet.removeAvt(uid);
        }
    },
    insertOrUpdateAvt:function(lat,lng,uid){
        if(AvatarSet.set.hasOwnProperty(uid)){
            var avt=AvatarSet.set[uid];
            avt.move(lat,lng);
            return avt;
        }
        var newAvt=new Avatar(lat,lng,uid.substr(uid.length-3),uid);
        $('#NbNbors')
            .html(''+
                AvatarSet.nbNbors());

        return newAvt;
    }

};

function Avatar(latitude,longitude,name,uid){
    console.log('-- add avatar',name);
//    var self = this;
    this.ll= new google.maps.LatLng(latitude, longitude);
    this.name = name;
    this.uid=uid;
    var strokeColor='blue';
    if(name === 'me'){ strokeColor='red';}
    this.marker = new google.maps.Marker({
        position: this.ll,
//        map: map,
        title:this.name,
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            strokeColor:strokeColor,
            scale: 3
        }
    });
    this.marker.setMap(map);
    AvatarSet.set[this.uid]=this;
}

Avatar.prototype = {
    latlng:function(latitude, longitude){
        latitude=parseFloat(latitude);
        longitude=parseFloat(longitude);
        this.latitude = latitude;
        this.longitude = longitude;
        var wraparound = false;
        return wraparound;
    },
    xy:function(x, y){
        x=parseFloat(x);
        y=parseFloat(y);
        var wraparound = false;
        return wraparound;
    },
    remove:function(){
        this.marker.setMap(null);
        this.marker = null;
    },
    move: function(lat,lng){
        this.marker.setPosition(new google.maps.LatLng(lat,lng));
    },
    movePx: function(x,y, withTransition){
        var wraparound = this.xy(x,y);
        if(wraparound ){ withTransition = false;}
        var self = this;
        if(withTransition){
            self.label
                .transition()
                .attr('x',self.x).attr('y',self.y)
                .ease('linear').duration(500)
                .each('end',function(){ self.label.attr('x',self.x).attr('y',self.y).attr('font-size', '10px');});
        } else {  self.label.attr('x',self.x).attr('y',self.y);   }
    },
    redraw:function(){

    }
};
