'use strict';
var google, map; // imported libs
var AvatarSet; // kiwano import from avatar.js

var KV;
KV ={
    zoomToViewAll:function(){
        var llb = new google.maps.LatLngBounds(AvatarSet.set[0].marker.getPosition(),
            AvatarSet.set[0].marker.getPosition() );
        for(var uid in AvatarSet.set) {
            if (AvatarSet.set.hasOwnProperty(uid)){
                llb.extend(AvatarSet.set[uid].marker.getPosition());
            }
        }
//        map.panToBounds(llb);
        map.fitBounds(llb);
    }
};
