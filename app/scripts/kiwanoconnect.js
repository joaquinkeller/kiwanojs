// # __KiwanoJS__
// ### Connecting [Kiwano](http://kiwano.li "Kiwano webpage") from Javascript

'use strict';
var KC; // "Kiwano Connect"
var KWS; // "Kiwano WebSocket"
var AvatarSet,SA;

// _Sample code on how to use [Kiwano](http://kiwano.li "Kiwano web page")_

KC = {
    init:function(lat,lng){
// First, make  a [jsonp](http://en.wikipedia.org/wiki/JSONP) call to be assigned a Kiwano proxy
        $.ajax({
                url :  'http://getproxy.kiwano.li',
                dataType: 'jsonp',
                success: function(jsondata){
                    KC.proxyurl = jsondata.url;
                    KC.connectws(KC.proxyurl,lat,lng);
                }
            });
    },
        // Then connect to the websocket providing the following parameters:

        //     lat, lng // the landing point
        //     urlid  // a unique id for this avatar
        //     iid  // a unique id for this avatar instance
    // _please read the [doc](http://kiwano.li/apidoc.html 'Documentation on the urlid scheme') on the urlid scheme_
    connectws:function(serverUrl,lat,lng){
        SA.init(lat,lng);  // Draw self avatar
        var urlid='http://hybridearth.net/u/anonymous';
        var iid=new Date();
        iid = 'kjs'+iid.getTime().toString();
        var wsuri = serverUrl+ '/?';
        wsuri += 'urlid=' + encodeURIComponent(urlid);
        wsuri += '&iid=' + iid.toString();
        wsuri += '&lat=' + lat.toString();
        wsuri += '&lng=' + lng.toString();
        KWS= KC.openWebsocket(wsuri);
// Kiwano issues 4 types of notifications:

//     neighbors, updates, removes, msg
        KWS.onmessage = function(e){
            //console.log("notification from kiwano:",e.data);
            var data = JSON.parse(e.data);
            //console.log(data.method);
            switch (data.method){
                case 'neighbors':
                    KC.neighbors(data);
                    break;
                case 'updates':
                    KC.updates(data);
                    break;
                case 'removes':
                    KC.removes(data);
                    break;
            }
        };
        KWS.onopen = function(){
            console.log('Connection open');
            $('#wsurl').html(KC.proxyurl.substr(5));
            //connection.send(JSON.stringify({ method:"neighbors" }));
            //connection.send(JSON.stringify({ method:"msg2all", msg:"Hello I'am "+iid}));
        };
        KWS.onclose = function(){
            console.log('Connection closed');
        };
        KWS.onerror = function(error){
            console.log('Error detected: ');
            console.log(error);
        };
    },
    neighbors:function(data){
        console.log('** neighbors');
        for(var uid in AvatarSet.set){if (AvatarSet.set.hasOwnProperty(uid)){
                AvatarSet.set[uid].keepIt=false;
            }
        }
        SA.avt.keepIt=true;   // keep self avatar !!!

        // Insert (or update) all received neighbors
        KC.updates(data);

        // Remove everything else
        for(uid in AvatarSet.set){if (AvatarSet.set.hasOwnProperty(uid)){
                if(!AvatarSet.set[uid].keepIt){AvatarSet.removeAvt(uid);}
            }
        }
    },
    updates:function(data){
        var nbors= data.nbors;

        for(var i=0, len=nbors.length; i < len; i++){
            //console.log('nbr',nbors[i].iid);
            var uid = nbors[i].urlid+nbors[i].iid;
            var avt = AvatarSet.insertOrUpdateAvt(nbors[i].lat, nbors[i].lng, uid);
            avt.keepIt=true;  // this is for neighbors method
        }
    },
    removes:function(data){
        var removes=data.removes;
        for(var i=0,len=removes.length;i<len;i++){
            console.log(removes[i]);
            var uid=removes[i][0]+removes[i][1];
            console.log('removing',uid);
            if (AvatarSet.set.hasOwnProperty(uid)){
                AvatarSet.removeAvt(uid);
            }
        }
    },
    openWebsocket:function(wsuri){
        console.log('tentative connection to ' + wsuri);
        if ('WebSocket' in window) {  //standard verification
            return new WebSocket(wsuri);
        }
        else if ('MozWebSocket' in window) {
            return new window.MozWebSocket(wsuri);
        }
        else {
            window.alert('Browser does not support WebSocket!');
            return false;
        }
    }
};