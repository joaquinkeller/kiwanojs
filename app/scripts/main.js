'use strict';
var map, google, overlay;
//var d3; // imported libs
var Avatar,KC, AvatarSet; // kiwano imports
var KV ; //Kiwano View
var KWS; // the websocket
var DEBUG={};

var SA;  // Self Avatar
SA={
    init:function(lat,lng){
        SA.avt=new Avatar(lat,lng,'me',0);
        SA.checkIsOut();
    },
    translatePx:function(dx,dy){
        var proj = overlay.getProjection();
        var ll = SA.avt.marker.getPosition();
        var p = proj.fromLatLngToContainerPixel(ll);
        console.log(p.x, p.y);
        p = new google.maps.Point(p.x+dx, p.y+dy);
        ll=proj.fromContainerPixelToLatLng(p);
        SA.updatePosition(ll.lat(),ll.lng());

        SA.avt.marker.setPosition(ll);
        $('#Latitude').html(ll.lat());
        $('#Longitude').html(ll.lng());

    },
    updatePosition:function(lat,lng){
        if(!KWS){ return ;}
        KWS.send(JSON.stringify({ method:'update', lat:lat, lng:lng }));
    },
    left:function(){
        console.log('left');
        SA.translatePx(-20,0);
    },
    up:function(){
        console.log('up');
        SA.translatePx(0,-20 );
    },
    right:function(){
        console.log('right');
        SA.translatePx(20,0);
    },
    down:function(){
        console.log('down');
        SA.translatePx(0,20);
    },
    checkIsOut:function(){

    },
    centerOnSelf:function(){
        map.panTo(SA.avt.marker.getPosition());
    },
    askNeighbors:function(){
        KWS.send(JSON.stringify({ method:'neighbors' }));
    },
    jumpToNearest:function(){
        var saLatLng = SA.avt.marker.getPosition();
        var nearestll=saLatLng; // self
        var nearest2ll=saLatLng; // self
        var dist = 40000000000;
        var d,ll;
        for(var uid in AvatarSet.set) {
            if (uid!=='0' && AvatarSet.set.hasOwnProperty(uid)){
                ll= AvatarSet.set[uid].marker.getPosition();
                d = google.maps.geometry.spherical.computeDistanceBetween(saLatLng,ll);
                if(d<dist){
                    dist = d;
                    nearest2ll =nearestll;
                    nearestll = ll;
                }
            }
        }
        ll = google.maps.geometry.spherical.interpolate(nearest2ll,nearestll,0.5);
        SA.avt.marker.setPosition(ll );
        $('#Latitude').html(ll.lat());
        $('#Longitude').html(ll.lng());
    }
};

$(function(){
    var startLat = 48.856916+Math.random()/1000;
    var startLng = 2.296861+Math.random()/1000;
    var mapOptions = {
        center: new google.maps.LatLng(startLat, startLng),
        zoom: 18
    };
    map = new google.maps.Map(document.getElementById('wf'), mapOptions);
    overlay = new google.maps.OverlayView();
    overlay.draw = function() {};
    overlay.setMap(map);
    $('#Latitude').html(startLat);
    $('#Longitude').html(startLng);

    $('#ScaleLevel').html(map.getZoom());
    google.maps.event.addListener(map, 'zoom_changed', function() {
        $('#ScaleLevel').html(map.getZoom());
    });

    $('#Jump_to_Nearest').click(SA.jumpToNearest);
    $('#Zoom_to_View_All').click(KV.zoomToViewAll);
    $('#Center_on_Self').click(SA.centerOnSelf);
    $('#Ask_Neighbors').click(SA.askNeighbors);

    $(window).keydown(function(e){
        DEBUG.e = e;
        switch (e.keyCode){
            case 37:
                SA.left();
                e.preventDefault();
                break;
            case 38:
                SA.up();
                e.preventDefault();
                break;
            case 39:
                SA.right();
                e.preventDefault();
                break;
            case 40:
                SA.down();
                e.preventDefault();
                break;

            default:
//                console.log(e.keyCode);
                break;
        }
    });
    // KV.init();
//    return
    $(window).on('resize',KV.resize);


    KC.init(startLat, startLng);

    console.log('OK');


});

